#ifndef DASHBOARD_1_H
#define DASHBOARD_1_H

#include <QtGui>
#include <QVBoxLayOut>
#include <QtGui/QMainWindow>
#include <QPushButton>
#include "ui_formMain.h"
#include "DashBoard.h"
#include "DashBoard_1.h"
#include <QWidget>
#include <QLineEdit>
#include <QFrame>
#include <QLabel>

class frmMain : public QMainWindow
{
	Q_OBJECT

public:
	frmMain(QWidget *parent = 0);
	~frmMain();

private slots:
	void changeValue();
	void changeTransparency();

private:
	Ui::MainWindow ui;
	QVBoxLayout *mainLayout;
	QPushButton *btn_2;
	QPushButton *btn_3;
	QLabel *label_1;
	QLineEdit *lineEdit_1;
	QPushButton *btn_1;
	DashBoard *dashBoard_1;
	//DashBoard_1 *dashBoard_1;
};

#endif // DASHBOARD_1_H
