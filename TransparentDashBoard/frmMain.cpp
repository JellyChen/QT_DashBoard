#include "frmMain.h"
#include "string"
using namespace std;

frmMain::frmMain(QWidget *parent)
	: QMainWindow(parent)
{
	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("GB2312"));
	ui.setupUi(this);
	mainLayout=new QVBoxLayout();

	btn_1 = new QPushButton();
	//btn_2 = new QPushButton();
	//btn_3 = new QPushButton();
	label_1 = new QLabel();
	//lineEdit_1 = new QLineEdit();
	//dashBoard_1 = new DashBoard_1(ui.frame);
	dashBoard_1 = new DashBoard(ui.frame);
	dashBoard_1->setGeometry(50, 50, 600, 600);
	dashBoard_1->setAnimating(true);

	mainLayout->addWidget(btn_1);
	//mainLayout->addWidget(btn_2);
	//mainLayout->addWidget(btn_3);
	mainLayout->addWidget(label_1);
	//mainLayout->addWidget(lineEdit_1);

	btn_1->setText("设置值");
	btn_1->setFixedHeight(50);
	//btn_2->setText("设置透明度");
	//btn_2->setFixedHeight(50);
	//btn_3->setText("Hello32");
	//btn_3->setFixedHeight(50);
	label_1->setText("当前值：");
	
	bool b1 = connect(btn_1, SIGNAL(clicked()), this, SLOT(changeValue()));
	//bool b2 = connect(btn_2, SIGNAL(clicked()), this, SLOT(changeTransparency()));

	ui.frame_2->setLayout(mainLayout);

}

frmMain::~frmMain()
{

}

void frmMain::changeValue()
{
	int value=qrand()%40 - 20;
	dashBoard_1->setCurrentValue(value);
	label_1->setText(tr("当前值:%1").arg(value));
}

void frmMain::changeTransparency()
{
}