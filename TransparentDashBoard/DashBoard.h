#ifndef DASHBOARD_H
#define DASHBOARD_H

#include <QtGui>
#include <QWidget>

class DashBoard : public QWidget
{
	Q_OBJECT

public:
	DashBoard(QWidget *parent);
	~DashBoard();
public:
	void setCurrentValue(qreal value);
	void setAnimating(bool enable);

	void setStartAngle(qreal value);
	void setEndAngle(qreal value);
	void setMaxValue(qreal value);
	void setMinValue(qreal value);
private slots:
	void UpdateGraph();
protected:
	// 绘制事件
	void paintEvent(QPaintEvent *);

private:
	// 初始化变量
	void initVariables();
	// 重置圆形相关变量
	void resetVariables(QPainter* painter);
	// 绘制外框
	void drawOuterRect(QPainter* painter);
	// 绘制彩色刻度条
	void drawColorCircle(QPainter* painter);
	// 绘制指针
	void drawIndicator(QPainter* painter);

private:
	// 外部矩形边长
	qreal m_outerRectLength;
	// 中心点
	QPointF m_centerT;
	// 基准点
	QPointF m_base;
	// 虚拟中心点
	QPointF m_centerF;
	// 彩色刻度条范围
	QRectF m_colorCircleRect;
	// 彩色刻度条半径
	qreal m_colorCircleRadius;
	// 彩色刻度条宽度
	qreal m_colorCircleWidth;
	// 彩色游标顶点半径
	qreal m_colorIndicRadius;
	// 彩色游标高
	qreal m_colorIndicHeight;
	// 彩色游标宽
	qreal m_colorIndicWidth;



	// 起始角度
	qreal startAngle;
	// 终点角度
	qreal endAngle;
	// 最小值
	qreal startValue;
	// 最大值
	qreal endValue;

	// 指针目标值
	qreal m_value;
	// 指针当前值
	qreal m_currentValue;



	bool m_bAnimating;
	QTimer* updateTimer;
	bool m_bReverse;
};

#endif // DASHBOARD_H
