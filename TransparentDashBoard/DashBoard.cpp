#include "DashBoard.h"

DashBoard::DashBoard(QWidget *parent)
	: QWidget(parent)
{
	this->initVariables();
	this->setAutoFillBackground(true);

	QPalette palette;
	palette.setColor(QPalette::Background, QColor(192,253,123));
	this->setPalette(palette);
}
DashBoard::~DashBoard()
{

}

void DashBoard::initVariables()
{
	// 画图基准点
	m_outerRectLength = width() > height() ? height() * 0.92 : width() * 0.92 ;
	m_centerF = QPointF(0, 0);
	m_centerT = QPointF(width()/2, height()/2);
	m_base = QPointF((width() - m_outerRectLength)/2, (height() - m_outerRectLength)/2);
	m_colorCircleRadius = m_outerRectLength/2 * 0.92;
	m_colorCircleWidth = m_colorCircleRadius * 0.08;
	m_colorIndicRadius = m_outerRectLength/2 * 0.85;
	m_colorIndicHeight = m_colorIndicRadius * 0.23;
	m_colorIndicWidth = m_colorIndicRadius * 0.08;

	// 画图动态点
	m_value = 0;
	m_currentValue = 0;

	startAngle = -90;
	endAngle = 90;
	startValue = -20;
	endValue = 20;

	// 更新图定时器
	updateTimer = new QTimer(this);
	updateTimer->setInterval(1);
	connect(updateTimer, SIGNAL(timeout()), this, SLOT(UpdateGraph()));
}


void DashBoard::paintEvent(QPaintEvent *)
{
	QPainter *painter = new QPainter(this);
	painter->setRenderHints(QPainter::Antialiasing|QPainter::TextAntialiasing);

	resetVariables(painter);
	drawOuterRect(painter);
	drawColorCircle(painter);
	drawIndicator(painter);

	painter->end();
}
void DashBoard::resetVariables(QPainter* painter)
{
	m_outerRectLength = width() > height() ? height() * 0.92 : width() * 0.92 ;
	m_centerF = QPointF(0, 0);
	m_centerT = QPointF(width()/2, height()/2);
	m_base = QPointF((width() - m_outerRectLength)/2, (height() - m_outerRectLength)/2);
	m_colorCircleRadius = m_outerRectLength/2 * 0.92;
	m_colorCircleWidth = m_colorCircleRadius * 0.08;
	m_colorIndicRadius = m_outerRectLength/2 * 0.85;
	m_colorIndicHeight = m_colorIndicRadius * 0.23;
	m_colorIndicWidth = m_colorIndicRadius * 0.08;
}
void DashBoard::drawOuterRect(QPainter* painter)
{
	painter->save();

	painter->drawPixmap(m_base.x(), m_base.y(), m_outerRectLength, m_outerRectLength, QPixmap("Images/DashBoard.png"));

	painter->restore();
}
void DashBoard::drawColorCircle(QPainter* painter)
{
	painter->save();
	painter->translate(m_centerT);

	qreal dAngle = (endAngle - startAngle) / (endValue - startValue);
	qreal currentAngle = (m_currentValue - startValue) * dAngle;

	QPointF topLeftPot(m_centerF.x() - m_colorCircleRadius, m_centerF.y() - m_colorCircleRadius);
	QPointF bottomRightPot(m_centerF.x() + m_colorCircleRadius, m_centerF.y() + m_colorCircleRadius);
	m_colorCircleRect=QRectF(topLeftPot,bottomRightPot);

	QPointF leftPot(m_centerF.x(),m_centerF.y() - m_colorCircleRadius);
	QPointF rightPot(m_centerF.x(),m_centerF.y() + m_colorCircleRadius);
	QLinearGradient indicatorGradient(leftPot,rightPot);
	indicatorGradient.setColorAt(0.0,QColor(0,255,0));
	indicatorGradient.setColorAt(0.5,QColor(255,255,0));
	indicatorGradient.setColorAt(1.0,QColor(255,0,0));

	painter->setPen(QPen(indicatorGradient, m_colorCircleWidth, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
	painter->drawArc(m_colorCircleRect, startAngle*16, currentAngle*16);

	painter->restore();
}
void DashBoard::drawIndicator(QPainter *painter)
{
	painter->save();

	painter->translate(m_centerT);

	qreal dAngle = (endAngle - startAngle) / (endValue - startValue);
	qreal currentAngle=270 - (m_currentValue - startValue) * dAngle - startAngle;
	painter->rotate(currentAngle);

	QPointF topPot(m_centerF.x(), m_colorIndicRadius);
	QPointF bottomPot(m_centerF.x(), m_colorIndicRadius + m_colorIndicHeight);
	QPointF bottomLeftPot(-m_colorIndicWidth, m_colorIndicRadius + m_colorIndicHeight);
	QPointF bottomRightPot(m_colorIndicWidth, m_colorIndicRadius + m_colorIndicHeight);
	painter->setPen(Qt::NoPen);

	QLinearGradient indicatorGradient(topPot,bottomPot);
	indicatorGradient.setColorAt(0.0,QColor(255,255,255));
	indicatorGradient.setColorAt(0.9,QColor(150,150,150));
	indicatorGradient.setColorAt(1.0,QColor(255,255,255));

	painter->setBrush(indicatorGradient);
	QVector<QPointF> potVec;
	potVec.push_back(topPot);
	potVec.push_back(bottomLeftPot);
	potVec.push_back(bottomRightPot);

	painter->drawPolygon(potVec);
	painter->restore();
}


void DashBoard::setStartAngle(qreal value)
{
	startAngle = value;
}
void DashBoard::setEndAngle(qreal value)
{
	endValue = value;
}
void DashBoard::setMaxValue(qreal value)
{
	startValue = value;
}
void DashBoard::setMinValue(qreal value)
{
	endValue = value;
}


void DashBoard::setAnimating(bool enable)
{
	m_bAnimating = enable;
	update();
}
void DashBoard::setCurrentValue(qreal value)
{
	if(value > m_currentValue)
	{
		m_bReverse = false;
		m_value = value;

		if(!m_bAnimating)
		{
			m_currentValue = m_value;
		}
	}
	else if(value < m_currentValue)
	{
		m_bReverse = true;
		m_value = value;
		if(!m_bAnimating)
		{
			m_currentValue = m_value;
		}
	}
	else
	{
		return ;
	}
	updateTimer->start();
}
void DashBoard::UpdateGraph()
{    
	if(m_bAnimating)
	{
		if(!m_bReverse)
		{
			m_currentValue += 0.5;

			if(m_currentValue >= m_value)
			{
				updateTimer->stop();
			}
		}
		else
		{
			m_currentValue -= 0.5;
			if(m_currentValue <= m_value)
			{
				updateTimer->stop();
			}
		}
	}
	update();
}