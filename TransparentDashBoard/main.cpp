#include "frmMain.h"
#include <QtGui/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	QTextCodec *codec = QTextCodec::codecForName("System"); 
	QTextCodec::setCodecForLocale(codec); 
	QTextCodec::setCodecForTr(codec);
	QTextCodec::setCodecForCStrings(codec); 

	frmMain w;
	w.show();
	return a.exec();
}
