#include "DashBoard_1.h"

DashBoard_1::DashBoard_1(QWidget *parent)
	:QWidget(parent)
{
	this->initVariables();
}

void DashBoard_1::initVariables()
{
	m_outerRectLength=width()>height() ? height(): width();
	m_outerRadius=m_outerRectLength/2;
	m_innerRadius=m_outerRadius/8*7;
	m_coverCircleRadius=m_outerRadius/8*4+10;
	m_colorCircleRadius=m_outerRadius/8*5;
	m_center=rect().center();
	m_value=0;
	m_currentValue=0;

	updateTimer=new QTimer(this);
	updateTimer->setInterval(1);
	connect(updateTimer,SIGNAL(timeout()),this,SLOT(UpdateGraph()));
}

void DashBoard_1::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	painter.setRenderHints(QPainter::Antialiasing|QPainter::TextAntialiasing);
	resetVariables(&painter);
	drawOuterRect(&painter);
	//drawOuterCircle(&painter);
	//drawInnerCircle(&painter);
	drawColorPie(&painter);
	drawCoverCircle(&painter);
	drawMark(&painter);
	drawIndicator(&painter);
	drawCoverBall(&painter);
	//drawTextRect(&painter);
	painter.end();
}

void DashBoard_1::drawTextRect(QPainter *painter)
{
	painter->save();
	qreal rectWidth=m_coverCircleRadius/5;

	QPointF topLeftPot(m_center.x()-4.5*rectWidth,m_center.y()-rectWidth*0.5);
	QPointF bottomRightPot(topLeftPot.x()+3*rectWidth,topLeftPot.y()+rectWidth*2);
	QRectF textRect(topLeftPot,bottomRightPot);

	painter->setPen(Qt::NoPen);
	painter->setBrush(QColor(0,170,255));
	painter->setOpacity(0.6);
	painter->drawRoundRect(textRect,ANGLE,ANGLE);

	qreal fontSize=textRect.height()/2;
	QFont font;
	font.setPointSize(fontSize);
	painter->setFont(font);

	painter->setOpacity(1.0);
	painter->setPen(QColor(0,0,0));
	QString strValue;
	strValue=tr("%1").arg(m_value);
	painter->drawText(textRect,Qt::AlignHCenter|Qt::AlignVCenter,strValue);
	painter->restore();
}

void DashBoard_1::drawCoverBall(QPainter *painter)
{
	painter->save();

	m_coverBallRadius=m_outerRadius/20;
	QRadialGradient ballGradient(m_center,m_coverBallRadius,m_center);
	ballGradient.setColorAt(0.0,QColor(140,140,140));
	ballGradient.setColorAt(0.7,QColor(140,140,140));
	ballGradient.setColorAt(1.0,QColor(60,60,60));
	painter->setBrush(QColor(0,255,255));
	painter->setPen(Qt::NoPen);
	painter->drawEllipse(m_center,m_coverBallRadius,m_coverBallRadius);

	painter->restore();
}

void DashBoard_1::drawIndicator(QPainter *painter)
{
	painter->save();

	painter->translate(m_center);
	qreal increment=(qreal)180/100;
	qreal changedAngle=270-increment*m_currentValue;
	painter->rotate(changedAngle);
	
	m_coverBallRadius = m_outerRadius/7;
	QPointF topPot(0,m_colorCircleRadius+LONG);
	QPointF bottomLeftPot(-m_coverBallRadius/10,0);
	QPointF bottomRightPot(m_coverBallRadius/10,0);
	painter->setPen(Qt::NoPen);

	QLinearGradient indicatorGradient(topPot,bottomLeftPot);
	indicatorGradient.setColorAt(0.0,QColor(0,0,255));
	indicatorGradient.setColorAt(0.5,QColor(0,255,0));
	indicatorGradient.setColorAt(1.0,QColor(255,0,0));

	painter->setBrush(QColor(0,255,255));
	QVector<QPointF> potVec;
	potVec.push_back(topPot);
	potVec.push_back(bottomLeftPot);
	potVec.push_back(bottomRightPot);

	painter->drawPolygon(potVec);
	painter->restore();
}

void DashBoard_1::drawMark(QPainter *painter)
{
	painter->save();
	painter->setPen(QColor(255,255,255));
	painter->translate(m_center);

	qreal dAngle=(qreal)180/100;
	qreal startAngle=180;
	int value=50;
	QString strValue;
	for(int i=100;i>=0;i--)
	{
		painter->save();
		painter->rotate(startAngle);

		if(i%10==0)
		{
			strValue=tr("%1").arg(value);
			qreal textWidth=fontMetrics().width(strValue);
			qreal textHeight=fontMetrics().height();
			QPointF bottomPot(0,m_colorCircleRadius+SPACE);
			QPointF topPot(0,m_colorCircleRadius+SPACE+LONG);
			painter->drawLine(bottomPot,topPot);
			value-=10;

			painter->save();
			QPointF textPot(0-textWidth/2,m_colorCircleRadius+SPACE+LONG+SPACE+textHeight);
			painter->translate(textPot);
			painter->rotate(180);
			painter->drawText(QPointF(-textWidth,textHeight/2),strValue);
			painter->restore();            
		}
		else if(i%5==0)
		{
			QPointF bottomPot(0,m_colorCircleRadius+SPACE);
			QPointF topPot(0,m_colorCircleRadius+SPACE+OK);
			painter->drawLine(bottomPot,topPot);

		}
		else
		{
			QPointF bottomPot(0,m_colorCircleRadius+SPACE);
			QPointF topPot(0,m_colorCircleRadius+SPACE+SHORT);
			painter->drawLine(bottomPot,topPot);
		}
		painter->restore();
		startAngle+=dAngle;
	}
	painter->restore();
}

void DashBoard_1::drawCoverCircle(QPainter* painter)
{
	painter->save();

	QPointF topLeftPot(m_center.x()-m_coverCircleRadius,m_center.y()-m_coverCircleRadius);
	QPointF bottomRightPot(m_center.x()+m_coverCircleRadius,m_center.y()+m_coverCircleRadius);
	m_coverCircleRadiusRect=QRectF(topLeftPot,bottomRightPot);

	QRadialGradient innerGradient(m_center,m_coverCircleRadius,m_center);
	innerGradient.setColorAt(0.0,QColor(255,255,255,100));
	innerGradient.setColorAt(0.99,QColor(255,255,255,100));
	innerGradient.setColorAt(1.0,QColor(255,255,255));

	painter->setBrush(innerGradient);
	painter->setPen(Qt::NoPen);
	painter->drawPie(m_coverCircleRadiusRect,-90*16,180*16);
	painter->restore();


	painter->save();
	painter->setPen(QPen(QColor(255,255,255), 1));
	painter->translate(m_center);
	painter->drawLine(0, 0, 0 + m_coverCircleRadius, 0);
	painter->setPen(QPen(QColor(255, 170, 0), 2));
	painter->drawLine(0, 0, 0, 0 + m_coverCircleRadius);
	painter->drawLine(0, 0, 0, 0 - m_coverCircleRadius);

	QVector<QPointF> potVec1;
	potVec1.push_back(QPointF(0, m_coverCircleRadius));
	potVec1.push_back(QPointF(-m_coverCircleRadius/50,m_coverCircleRadius - 5));
	potVec1.push_back(QPointF(m_coverCircleRadius/50,m_coverCircleRadius - 5));
	QVector<QPointF> potVec2;
	potVec2.push_back(QPointF(0, -m_coverCircleRadius));
	potVec2.push_back(QPointF(-m_coverCircleRadius/50,-m_coverCircleRadius + 5));
	potVec2.push_back(QPointF(m_coverCircleRadius/50,-m_coverCircleRadius + 5));

	painter->setPen(Qt::NoPen);
	painter->setBrush(QColor(255, 255, 0));

	painter->drawPolygon(potVec1);
	painter->drawPolygon(potVec2);

	painter->restore();
}

void DashBoard_1::drawColorPie(QPainter* painter)
{
	painter->save();

	QPointF topLeftPot(m_center.x()-m_colorCircleRadius + 15,m_center.y()-m_colorCircleRadius + 15);
	QPointF bottomRightPot(m_center.x()+m_colorCircleRadius - 15,m_center.y()+m_colorCircleRadius - 15);
	m_colorCircleRect=QRectF(topLeftPot,bottomRightPot);

	QPointF leftPot(m_center.x(),m_center.y()-m_colorCircleRadius);
	QPointF rightPot(m_center.x(),m_center.y()+m_colorCircleRadius);
	QLinearGradient indicatorGradient(leftPot,rightPot);
	indicatorGradient.setColorAt(0.0,QColor(0,255,0));
	indicatorGradient.setColorAt(0.5,QColor(255,255,0));
	indicatorGradient.setColorAt(1.0,QColor(255,0,0));

	painter->setPen(QPen(indicatorGradient, 20, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
	painter->drawArc(m_colorCircleRect, -90*16, 180*16);

	painter->restore();
}

void DashBoard_1::drawInnerCircle(QPainter* painter)
{
	painter->save();

	QRadialGradient innerGradient(m_center,m_innerRadius,m_center);
	innerGradient.setColorAt(0.0,QColor(130,130,130));
	innerGradient.setColorAt(0.69,QColor(130,130,130));
	innerGradient.setColorAt(0.7,QColor(130,130,130));
	innerGradient.setColorAt(1.0,QColor(80,80,80));

	painter->setPen(Qt::NoPen);
	painter->setBrush(innerGradient);
	painter->drawEllipse(m_center,m_innerRadius,m_innerRadius);

	painter->restore();
}

void DashBoard_1::drawOuterCircle(QPainter* painter)
{
	painter->save();

	QRadialGradient *outerGradient = new QRadialGradient(m_center, m_outerRadius, m_center);
	outerGradient->setColorAt(0.0,QColor(200,200,200));
	outerGradient->setColorAt(0.8999999,QColor(200,200,200));
	outerGradient->setColorAt(0.9,QColor(80,80,80));
	outerGradient->setColorAt(0.95,QColor(0,0,255));
	outerGradient->setColorAt(1.0,QColor(80,80,80));
	

	QPointF topLeftPot(m_center.x()-m_outerRadius,m_center.y()-m_outerRadius);
	QPointF bottomRightPot(m_center.x()+m_outerRadius,m_center.y()+m_outerRadius);
	m_outerRadiusRect=QRectF(topLeftPot,bottomRightPot);

	painter->setPen(Qt::NoPen);
	painter->setBrush(*outerGradient);
	painter->drawEllipse(m_center,m_outerRadius,m_outerRadius);
	//painter->drawPie(m_outerRadiusRect, 0*16, 180*16);
	painter->restore();

}

void DashBoard_1::drawOuterRect(QPainter* painter)
{
	painter->save();

	//QPixmap *pixmapBackground;
	//pixmapBackground=new QPixmap(":/SpecSituation/Images/home_background.png");
	//painter->drawPixmap(0,0,width(),height(),*pixmapBackground);

	QRadialGradient *outerGradient = new QRadialGradient(m_center, m_outerRadius, m_center);
	outerGradient->setColorAt(0.0,QColor(200,200,200));
	outerGradient->setColorAt(0.8999999,QColor(200,200,200));
	outerGradient->setColorAt(0.9,QColor(80,80,80));
	outerGradient->setColorAt(0.95,QColor(0,0,255));
	outerGradient->setColorAt(1.0,QColor(80,80,80));


	QPointF topLeftPot(m_center.x()-m_outerRadius,m_center.y()-m_outerRadius);
	QPointF bottomRightPot(m_center.x()+m_outerRadius,m_center.y()+m_outerRadius);
	m_outerRadiusRect=QRectF(topLeftPot,bottomRightPot);

	painter->setPen(Qt::NoPen);
	painter->setBrush(*outerGradient);
	painter->drawRect(0, 0, m_outerRectLength, m_outerRectLength);
	//painter->drawPie(m_outerRadiusRect, 0*16, 180*16);
	painter->restore();
}

void DashBoard_1::resetVariables(QPainter* painter)
{
	m_outerRectLength=width()>height() ? height(): width();
	m_outerRadius=width()>height() ? height()/2: width()/2;
	m_innerRadius=m_outerRadius*0.91;
	//m_innerRadius=m_outerRadius*bili;
	m_coverCircleRadius=m_outerRadius/8*4+10;
	m_colorCircleRadius=m_outerRadius/8*5;

	m_center=QPointF(m_outerRectLength/2, m_outerRectLength/2);
}

void DashBoard_1::setAnimating(bool enable)
{
	m_bAnimating=enable;
	update();
}

void DashBoard_1::setValue(qreal value)
{
	if(value>m_currentValue)
	{
		m_bReverse=false;
		m_value=value;

		if(!m_bAnimating)
		{
			m_currentValue=m_value;
		}
	}
	else if(value<m_currentValue)
	{
		m_bReverse=true;
		m_value=value;
		if(!m_bAnimating)
		{
			m_currentValue=m_value;
		}
	}
	else
	{
		return ;
	}
	updateTimer->start();
}

void DashBoard_1::UpdateGraph()
{    
	if(m_bAnimating)
	{
		if(!m_bReverse)
		{
			m_currentValue+=0.5;

			if(m_currentValue>=m_value)
			{
				updateTimer->stop();
			}
		}
		else
		{
			m_currentValue-=0.5;
			if(m_currentValue<=m_value)
			{
				updateTimer->stop();
			}
		}
	}
	update();
}