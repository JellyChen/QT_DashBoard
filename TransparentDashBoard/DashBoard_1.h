#pragma once

#define LONG 10
#define OK 7
#define SHORT 5
#define SPACE 3
#define ANGLE 10
#define TRANSPARENCY 0

#include <QWidget>
#include <QtGui>
#include <QPainter>

// 仪表盘
class DashBoard_1 : public QWidget
{
	Q_OBJECT
public:
	DashBoard_1(QWidget *parent = 0);
public slots:
	void setValue(qreal value);
	void setAnimating(bool enable);
protected:
	// 绘制事件
	void paintEvent(QPaintEvent *);
private:
	// 重置圆形相关变量
	void resetVariables(QPainter* painter);
	// 绘制外框
	void drawOuterRect(QPainter* painter);
	// 绘制外圆
	void drawOuterCircle(QPainter* painter);
	// 绘制内圆
	void drawInnerCircle(QPainter* painter);
	// 绘制彩色半圆面板
	void drawColorPie(QPainter* painter);
	// 绘制底色覆盖
	void drawCoverCircle(QPainter* painter);
	// 绘制刻度线
	void drawMark(QPainter* painter);
	// 绘制指针
	void drawIndicator(QPainter* painter);
	// 绘制中心小球
	void drawCoverBall(QPainter* painter);
	// 绘制显示值文本框
	void drawTextRect(QPainter* painter);

private:
	// 初始化变量
	void initVariables();

private slots:
	void UpdateGraph();

private:
	// 外部矩形边长
	qreal m_outerRectLength;
	// 外圆半径
	qreal m_outerRadius;
	// 外圆半径范围
	QRectF m_outerRadiusRect;
	// 内圆半径
	qreal m_innerRadius;
	// 底色覆盖半径
	qreal m_coverCircleRadius;
	// 底色覆盖范围
	QRectF m_coverCircleRadiusRect;
	// 彩色半圆半径
	qreal m_colorCircleRadius;
	qreal m_coverBallRadius;
	// 中心点
	QPointF m_center;
	// 指针目标值
	qreal m_value;
	// 指针当前值
	qreal m_currentValue;
	// 彩色半圆范围
	QRectF m_colorCircleRect;

	bool m_bAnimating;
	
	QTimer* updateTimer;

	bool m_bReverse;
};